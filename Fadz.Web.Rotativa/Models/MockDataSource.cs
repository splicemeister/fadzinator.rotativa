﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fadz.Web.Rotativa.Models
{
    ///<summary>
    ///MockDataSource, Replace it with your actuall data source
    ///By:Fadz @ www.fadz.net
    ///</summary>
    public class MockDataSource
    {
        public List<Fruit> GetAllFruits()
        {
            var fruits = new List<Fruit>();
            var fruit1 = new Fruit()
            {
                Id = 1,
                Name = "Mango",
                Price = 80.00,
                Quantity = 20
            };
            fruits.Add(fruit1);

            var fruit2 = new Fruit()
            {
                Id = 2,
                Name = "Banana",
                Price = 65.00,
                Quantity = 10
            };
            fruits.Add(fruit2);

            var fruit3 = new Fruit()
            {
                Id = 3,
                Name = "Watermelon",
                Price = 100.00,
                Quantity = 11
            };
            fruits.Add(fruit3);

            var fruit4 = new Fruit()
            {
                Id = 4,
                Name = "Grapes",
                Price = 400.00,
                Quantity = 5
            };
            fruits.Add(fruit4);

            var fruit5 = new Fruit()
            {
                Id = 5,
                Name = "Guyabano",
                Price = 40.00,
                Quantity = 8
            };
            fruits.Add(fruit5);

            return fruits;
        }
    }

}