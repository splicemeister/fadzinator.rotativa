﻿using Fadz.Web.Rotativa.Models;
using Rotativa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fadz.Web.Rotativa.Controllers
{
    public class HomeController : Controller
    {
        MockDataSource dataSource;

        public HomeController()
        {
            dataSource = new MockDataSource();
        }
        public ActionResult Index()
        {
            return View(dataSource.GetAllFruits());
        }

        public ActionResult ExportToPdfIndex()
        {
            return new ActionAsPdf("Index") { FileName = "Index.pdf" };
        }

        public ActionResult DataOnly()
        {
            return View(dataSource.GetAllFruits());
        }

        public ActionResult ExportToPdfDataOnly()
        {
            return new ActionAsPdf("DataOnly") { FileName = "DataOnly.pdf" };
        }

        public ActionResult DataWithImage()
        {
            return View(dataSource.GetAllFruits());
        }

        public ActionResult ExportToPdfDataWithImage()
        {
            return new ActionAsPdf("DataWithImage") { FileName = "DataWithImage.pdf" };
        }

        public ActionResult ViewAsPdfDataWithImage()
        {
            return new ViewAsPdf("DataWithImage", dataSource.GetAllFruits());
        }

        public ActionResult ViewDataWithPageBreak()
        {
            return new ViewAsPdf("DataWithPageBreak", dataSource.GetAllFruits());
        }
        public ActionResult Contact()
        {
            return View();
        }


    }
}